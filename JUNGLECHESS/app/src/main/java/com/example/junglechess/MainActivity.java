package com.example.junglechess;

import androidx.appcompat.app.AppCompatActivity;
import androidx.constraintlayout.widget.ConstraintLayout;

import android.graphics.Color;
import android.media.Image;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity  {

    ImageView [][] imgpapan = new ImageView[9][7];
    Papan [][] papan = new Papan[9][7];
    ConstraintLayout main_layout;
    String player1, player2;
    int x=50,y=10;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        main_layout= findViewById(R.id.main_layout);
        setlayoutPapan();
        setPosisiAwal();
        Character_Onclick();


    }

    int pos_lama_i=0;
    int pos_lama_j=0;
    private void Character_Onclick() {
        for (int i=0; i<9; i++){
            for (int j=0; j<7; j++){
                final int finalI = i;
                final int finalJ = j;
                imgpapan[i][j].setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       //gatau jlannya gimana
                    }
                });
            }
        }
    }

    private void setPosisiAwal() {
        for (int i=0; i<9; i++){
            for (int j=0;j<7;j++){
                papan[i][j]= new Papan("tanah");

            }
        }

        papan[0][2].type="trap";
        papan[0][3].type="goal1";
        papan[1][3].type="trap";
        papan[0][4].type="trap";

        papan[8][2].type="trap";
        papan[7][3].type="trap";
        papan[8][3].type="goal2";
        papan[8][4].type="trap";

        //menentukan letak water
        for (int i=3; i<6; i++){
            for (int j=1;j<3; j++){
                papan[i][j].type= "water";
            }
        }

        //menentukan letak water
        for (int i=3; i<6; i++){
            for (int j=4;j<6; j++){
                papan[i][j].type= "water";
            }
        }

        //menentukan posisi awal character per pemain
        player1="p1";
        papan[0][0].characters= new Lion(player1);
        papan[0][6].characters= new Tiger(player1);
        papan[1][1].characters= new Dog(player1);
        papan[1][5].characters= new Cat(player1);
        papan[2][0].characters= new Rat(player1);
        papan[2][2].characters= new Cheetah(player1);
        papan[2][4].characters= new Wolf(player1);
        papan[2][6].characters= new Elephant(player1);

        player2="p2";
        papan[8][6].characters= new Lion(player2);
        papan[8][0].characters= new Tiger(player2);
        papan[7][5].characters= new Dog(player2);
        papan[7][1].characters= new Cat(player2);
        papan[6][6].characters= new Rat(player2);
        papan[6][4].characters= new Cheetah(player2);
        papan[6][2].characters= new Wolf(player2);
        papan[6][0].characters= new Elephant(player2);


        //pasang gambarnya
        for (int i=0; i<9;i++){
            for (int j=0; j<7;j++){
                if (papan[i][j].type.equalsIgnoreCase("tanah")){
                    imgpapan[i][j].setBackgroundColor(Color.rgb(124,252,0));
                }
                if (papan[i][j].type.equalsIgnoreCase("water")){
                    imgpapan[i][j].setBackgroundColor(Color.rgb(176,224,230));
                }
                if (papan[i][j].type.equalsIgnoreCase("trap")){
                    imgpapan[i][j].setImageResource(R.drawable.ic_trap);
                    imgpapan[i][j].setBackgroundColor(Color.rgb(152,251,152));
                }
                if (papan[i][j].type.equalsIgnoreCase("goal1")){
                    imgpapan[i][j].setImageResource(R.drawable.goal1);
                }
                if (papan[i][j].type.equalsIgnoreCase("goal2")){
                    imgpapan[i][j].setImageResource(R.drawable.goal2);
                }
                setImage_Character( i,  j);


            }
        }

    }

    private void setImage_Character(int i, int j) {
        //menentukan warnanya
        if (papan[i][j].characters!=null){
            if (papan[i][j].characters.pemilik.equalsIgnoreCase(player2)){
                imgpapan[i][j].setBackgroundColor(Color.rgb(70,130,180));
            }
            if (papan[i][j].characters.pemilik.equalsIgnoreCase(player1)){
                imgpapan[i][j].setBackgroundColor(Color.rgb(178,34,34));
            }
        }

        if (papan[i][j].characters instanceof Rat){
            imgpapan[i][j].setImageResource(R.drawable.rat);
        }
        if (papan[i][j].characters instanceof Cat){
            imgpapan[i][j].setImageResource(R.drawable.cat);
        }
        if (papan[i][j].characters instanceof Dog){
            imgpapan[i][j].setImageResource(R.drawable.dog);
        }
        if (papan[i][j].characters instanceof Cheetah){
            imgpapan[i][j].setImageResource(R.drawable.leopard);
        }
        if (papan[i][j].characters instanceof Wolf){
            imgpapan[i][j].setImageResource(R.drawable.wolf);
        }
        if (papan[i][j].characters instanceof Lion){
            imgpapan[i][j].setImageResource(R.drawable.lion);
        }
        if (papan[i][j].characters instanceof Tiger){
            imgpapan[i][j].setImageResource(R.drawable.tiger);
        }
        if (papan[i][j].characters instanceof Elephant){
            imgpapan[i][j].setImageResource(R.drawable.elephant);
        }
    }

    private void setlayoutPapan() {
        for (int i =0; i<9; i++){
            for (int j=0;j<7; j++){
                imgpapan[i][j]= new ImageView(this);
                imgpapan[i][j].setLayoutParams(new ConstraintLayout.LayoutParams(130,130));
                //imgpapan[i][j].setImageResource(R.drawable.dog);
                imgpapan[i][j].setX(x);
                imgpapan[i][j].setY(y);
                x+=140;
                main_layout.addView(imgpapan[i][j]);
            }
            y+=140;
            x=50;
        }
    }

}
