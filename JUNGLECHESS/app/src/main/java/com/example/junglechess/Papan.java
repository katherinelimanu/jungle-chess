package com.example.junglechess;

import java.util.List;

public class Papan {
    Character characters;
    String type;

    public Papan() {
    }

    public Character getCharacters() {
        return characters;
    }

    public void setCharacters(Character characters) {
        this.characters = characters;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Papan(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "Papan{" +
                "characters=" + characters +
                ", type='" + type + '\'' +
                '}';
    }
}
